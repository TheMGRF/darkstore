package me.MFN.darkstore;

import net.md_5.bungee.api.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.metadata.FixedMetadataValue;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class DarkStore {

    private Main main = Main.getInstance();
    private FixedMetadataValue store;

    public DarkStore() {
        store = new FixedMetadataValue(main, 0);
    }

    public String getGuiName() {
        return main.getGuiName();
    }

    public void openStoreGui(Player player) {

        if (player.hasMetadata("store")) return;
        player.setMetadata("store", store);

        player.setMetadata("storemenu", new FixedMetadataValue(main, 0)); // main menu

        // display Store GUI to player
        Inventory inv = main.getServer().createInventory(null, 54, main.getGuiName());

        main.addFooter(player, inv);

        for (StoreCategory c : main.getStoreCategories()) {
            inv.setItem(c.getGuiSlot(), c.getIcon());
        }

        player.openInventory(inv);
        player.playSound(player.getLocation(), Sound.UI_BUTTON_CLICK, 1, 1);
        player.removeMetadata("store", main);

    }

    public void openCategoryGui(Player player, StoreCategory category, long premium, long cosmetic) {

        if (category == null) return;

        // show category menu
        player.setMetadata("storemenu", new FixedMetadataValue(main, 1));
        player.setMetadata("storecategory", new FixedMetadataValue(main, category.getGuiSlot()));

        Inventory inv = main.getServer().createInventory(null, 54, main.getGuiName() + " §f§l> §6§l" + category.getName());
        player.playSound(player.getLocation(), Sound.UI_BUTTON_CLICK, 1, 1);

        main.addFooter(player, inv);

        ItemMeta meta;
        List<String> lore = new ArrayList<>();

        ItemStack back = new ItemStack(Material.ARROW);
        meta = back.getItemMeta();
        meta.setDisplayName(main.format("&7Back"));
        back.setItemMeta(meta);
        inv.setItem(45, back);

        for (Map.Entry<Integer, StoreItem> entry : category.getStoreItems().entrySet()) {

            StoreItem storeItem = entry.getValue();

            ItemStack icon = storeItem.getIcon();
            meta = icon.getItemMeta();
            if (!meta.hasDisplayName()) {
                meta.setDisplayName(main.format("&e&n" + main.convertItemMaterial(icon.getType())));
            }
            if (meta.hasLore()) {
                lore = meta.getLore();
            } else {
                lore.clear();
            }
            lore.add("");
            lore.add(main.format(" &6&lInformation"));
            if (storeItem.hasPermission(player)) {

                boolean sale = false;
                ItemSale itemSale = null;
                long price = storeItem.getBuyPrice();
                if ((sale = main.isOnSale(storeItem.getName()))) {
                    itemSale = main.getItemSale(storeItem.getName());
                    meta.setDisplayName(meta.getDisplayName() + " " + main.format("&7&l(&e&lSALE! &6&l" + itemSale.getSalePercentage() + "% OFF!&7&l)"));

                    price = (long) Math.ceil((1.0 - (itemSale.getSalePercentage() / 100.0)) * price);
                }

                if (storeItem.isPremium()) {
                    String showPrice = main.getPremiumCurrencySymbol() + main.getPremiumCurrencyColour() + NumberFormat.getInstance().format(storeItem.getBuyPrice());
                    if (sale) {
                        showPrice = ChatColor.RED + "" + ChatColor.STRIKETHROUGH + ChatColor.stripColor(showPrice) + ChatColor.RESET + " " + main.getPremiumCurrencySymbol() + main.getPremiumCurrencyColour() + NumberFormat.getInstance().format(price);
                    }
                    lore.add(main.format("  &7Cost: " + showPrice));

                    if (premium >= storeItem.getBuyPrice()) {
                        lore.add("");
                        lore.add(main.format("&6&l(!) &eLeft Click to buy!"));
                    } else {
                        lore.add(ChatColor.LIGHT_PURPLE + "You need " + main.getPremiumCurrencySymbol() + main.getPremiumCurrencyColour() + NumberFormat.getInstance().format((price - premium)) + ChatColor.LIGHT_PURPLE + " more " + main.getPremiumCurrencyName() + ChatColor.LIGHT_PURPLE + "!");
                    }
                } else if (storeItem.isCosmetic()) {
                    String showPrice = main.getCosmeticCurrencySymbol() + main.getCosmeticCurrencyColour() + NumberFormat.getInstance().format(storeItem.getBuyPrice());
                    if (sale) {
                        showPrice = ChatColor.RED + "" + ChatColor.STRIKETHROUGH + ChatColor.stripColor(showPrice) + ChatColor.RESET + " " + main.getCosmeticCurrencySymbol() + main.getCosmeticCurrencyColour() + NumberFormat.getInstance().format(price);
                    }
                    lore.add(main.format("  &7Cost: " + showPrice));

                    if (cosmetic >= storeItem.getBuyPrice()) {
                        lore.add("");
                        lore.add(main.format("&6&l(!) &eLeft Click to buy!"));
                    } else {
                        lore.add(ChatColor.LIGHT_PURPLE + "You need " + main.getCosmeticCurrencySymbol() + main.getCosmeticCurrencySymbol() + NumberFormat.getInstance().format((price - premium)) + ChatColor.LIGHT_PURPLE + " more " + main.getCosmeticCurrencyName() + ChatColor.LIGHT_PURPLE + "!");
                    }
                } else {
                    if (EssentialsHook.getEss() != null) {
                        //long money = player.getMetadata("money").get(0).asLong();
                        BigDecimal m = EssentialsHook.getBalance(player);

                        String showPrice = main.format(NumberFormat.getInstance().format(storeItem.getBuyPrice()));
                        if (sale) {
                            showPrice = ChatColor.RED + "" + ChatColor.STRIKETHROUGH + ChatColor.stripColor(showPrice) + ChatColor.RESET + " " + ChatColor.WHITE + "$" + ChatColor.GRAY + NumberFormat.getInstance().format(price);
                        }
                        lore.add(main.format("  &7Buy &f1 &7for &a$" + showPrice));

                        if (m.doubleValue() >= storeItem.getBuyPrice()) {
                            lore.add("");
                            lore.add(main.format(" &6&l(!) &eLeft Click to buy!"));
                        } else {
                            lore.add("");
                            lore.add(main.format(" &4&l(!) &cYou need &a$" + NumberFormat.getInstance().format(price - m.doubleValue()) + " &cmore money!"));
                            //lore.add(ChatColor.LIGHT_PURPLE + "You need " + ChatColor.WHITE + "$" + ChatColor.GRAY + NumberFormat.getInstance().format(price - m.doubleValue()) + ChatColor.LIGHT_PURPLE + " more money!");
                        }

                        BigDecimal sellPrice = EssentialsHook.getItemWorth(storeItem.getPurchaseItem());

                        if (storeItem.canSell() && sellPrice != null) {

                            lore.add(ChatColor.GRAY + "" + ChatColor.STRIKETHROUGH + "                                 ");
                            lore.add("");
                            lore.add(main.format(" &b&lInformation"));

                            // sell price
                            // only show if player has any of this item to sell
                            // if they do, this is only a "sell all" option and will display how many the player hsa to sell and the total price
                            if (player.getInventory().containsAtLeast(storeItem.getPurchaseItem(), 1)) {

                                ItemStack purchaseItem = storeItem.getPurchaseItem();
                                int amount = 0;
                                for (ItemStack is : player.getInventory().getContents()) {
                                    if (is != null && is.isSimilar(purchaseItem)) amount += is.getAmount();
                                }

                                // get total sell price
                                BigDecimal totalSellPrice = sellPrice.multiply(new BigDecimal(amount));

                                lore.add(main.format("  &7Sell &f" + NumberFormat.getInstance().format(amount) + " &7for &a$" + totalSellPrice.toPlainString()));
                                lore.add("");
                                lore.add(main.format(" &3&l(!) &bRight Click to sell!"));
                            } else {
                                lore.add(main.format("  &7Sell &f1 &7for &a$" + sellPrice));
                                lore.add("");
                                lore.add(main.format(" &4&l(!) &cYou have none of this item to sell"));
                            }

                        }
                    }
                }
            } else {
                lore.add(main.format("  &4&l(!) &cYou cannot purchase this item!"));
            }
            meta.setLore(lore);
            icon.setItemMeta(meta);

            inv.setItem(entry.getKey(), icon);

        }

        player.openInventory(inv);

    }
}
