package me.MFN.darkstore;

import com.earth2me.essentials.Essentials;
import com.earth2me.essentials.api.NoLoanPermittedException;
import com.earth2me.essentials.api.UserDoesNotExistException;
import net.ess3.api.Economy;
import net.ess3.api.MaxMoneyException;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.math.BigDecimal;

public class EssentialsHook {

    private static Essentials ess;

    public static boolean setup() {
        if (Bukkit.getPluginManager().isPluginEnabled("Essentials")) {
            ess = (Essentials) Bukkit.getPluginManager().getPlugin("Essentials");
            return true;
        } else {
            return false;
        }
    }

    public static Essentials getEss() { return ess; }

    public static BigDecimal getBalance(Player player) {
        BigDecimal money;
        if (getEss() != null) {
            try {
                money = Economy.getMoneyExact(player.getName());
            } catch (UserDoesNotExistException e) {
                e.printStackTrace();
                return BigDecimal.valueOf(-1);
            }
        } else {
            money = BigDecimal.valueOf(-1);
        }

        return money;
    }

    public static void giveMoney(Player player, BigDecimal amount) {
        if (getEss() != null) {
            try {
                EssentialsHook.getEss().getUser(player).giveMoney(amount);
            } catch (MaxMoneyException e1) {
                e1.printStackTrace();
            }
        }
    }

    public static void takeMoney(String username, BigDecimal amount) {
        if (getEss() != null) {
            try {
                Economy.substract(username, amount);
            } catch (NoLoanPermittedException | ArithmeticException | UserDoesNotExistException e1) {
                e1.printStackTrace();
            }
        }
    }

    public static BigDecimal getItemWorth(ItemStack itemStack) {
        if (getEss() != null) {
            try {
                return EssentialsHook.getEss().getWorth().getPrice(EssentialsHook.getEss(), itemStack);
            } catch (Exception ex) {
                return BigDecimal.valueOf(0);
            }
        }
        return BigDecimal.valueOf(0);
    }

}
