package me.MFN.darkstore;

public class ItemSale {
	
	private int salePercentage;
	private long finishTime;
	
	public ItemSale(int salePercentage, long finishTime) {
		this.salePercentage = salePercentage;
		this.finishTime = finishTime;
	}
	
	public int getSalePercentage() { return salePercentage; }
	public long getFinishTime() { return finishTime; }
	
	public void setFinishTime(long finishTime) { this.finishTime = finishTime; }
	
}
