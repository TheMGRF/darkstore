package me.MFN.darkstore;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.time.Instant;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import com.deanveloper.skullcreator.SkullCreator;
import me.MFN.darkstore.commands.*;
import me.themgrf.arcadecoinsapi.ArcadeCoinsAPI;
import me.themgrf.crystalsapi.CrystalsAPI;
import net.minecraft.server.v1_14_R1.NBTTagCompound;
import net.minecraft.server.v1_14_R1.NBTTagString;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.craftbukkit.v1_14_R1.inventory.CraftItemStack;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.plugin.ServicePriority;
import org.bukkit.plugin.java.JavaPlugin;

import me.MFN.darkstore.listeners.InventoryClickListener;
import me.MFN.darkstore.listeners.PlayerQuitListener;
import me.MFN.darkstore.guis.CheckoutGUI;
import net.antfarms.serviceconnector.ServiceConnector;

public class Main extends JavaPlugin {
	
	private ServiceConnector sc;
	private CrystalsAPI crystals;
	private ArcadeCoinsAPI arcadeCoins;
	
	private static DarkStore darkStore;

	private Map<String, StoreItem> storeItems = new HashMap<String, StoreItem>();
	private Map<Integer, StoreCategory> categories = new HashMap<Integer, StoreCategory>();
	private Map<String, ItemSale> sales = new HashMap<String, ItemSale>();
	
	private String guiName, storeLink, premiumCurrencyName, cosmeticCurrencyName;
	private String premiumCurrencySymbol, cosmeticCurrencySymbol;
	private ChatColor premiumCurrencyColour, premiumSymbolColour, cosmeticCurrencyColour, cosmeticSymbolColour;

	private File purchasesYmlFile, userSettingsYmlFile;
	private YamlConfiguration purchases, userSettings;

	private Set<Player> notifyPurchasesPlayers = new HashSet<Player>();

	public static Main instance;

	public void onEnable() {
		reload();

		instance = this;

		sc = getServer().getServicesManager().getRegistration(ServiceConnector.class).getProvider();
		crystals = getServer().getServicesManager().getRegistration(CrystalsAPI.class).getProvider();
		arcadeCoins = getServer().getServicesManager().getRegistration(ArcadeCoinsAPI.class).getProvider();

		darkStore = new DarkStore();

		System.out.println("Loading essentials!");
		if (Bukkit.getPluginManager().getPlugin("Essentials") != null) {
			System.out.println("Loaded essentials!");
			EssentialsHook.setup();
		} else {
			System.out.println("Loading essentials failed! Essentials is null!");
		}

		//getCommand("sell").setExecutor(new SellCmd(this)); // sell is handled by Essentials
		if (Bukkit.getPluginManager().isPluginEnabled("Essentials")) {
			getCommand("balance").setExecutor(new BalanceCmd(this));
		}
		getCommand("purchases").setExecutor(new PurchasesCmd(this));
		getCommand("sales").setExecutor(new SalesCmd(this));
		getCommand("shop").setExecutor(new ShopCmd(this));
		getCommand("shopspy").setExecutor(new ShopSpyCmd(this));
		getCommand("store").setExecutor(new StoreCmd(this));

		purchasesYmlFile = new File("plugins/DarkStore/purchases.yml");
		if (!purchasesYmlFile.exists())
			try {
				purchasesYmlFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		purchases = YamlConfiguration.loadConfiguration(purchasesYmlFile);
		if (!purchases.isConfigurationSection("purchases")) purchases.createSection("purchases");
		if (!purchases.isConfigurationSection("users")) purchases.createSection("users");
		try {
			purchases.save(purchasesYmlFile);
		} catch (IOException e) {
			e.printStackTrace();
		}

		userSettingsYmlFile = new File("plugins/DarkStore/usersettings.yml");
		if (!userSettingsYmlFile.exists())
			try {
				userSettingsYmlFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		userSettings = YamlConfiguration.loadConfiguration(userSettingsYmlFile);
		if (!userSettings.isConfigurationSection("users")) userSettings.createSection("users");
		try {
			userSettings.save(userSettingsYmlFile);
		} catch (IOException e) {
			e.printStackTrace();
		}

		getServer().getPluginManager().registerEvents(new InventoryClickListener(this, new CheckoutGUI(this), purchases), this);
		getServer().getPluginManager().registerEvents(new PlayerQuitListener(this), this);
		
		//getServer().getServicesManager().register(DarkStore.class, darkStore = new DarkStore(this), this, ServicePriority.Normal);
		
		// repeating task to track when sales end
		getServer().getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
			
			Map<String, ItemSale> s = new HashMap<String, ItemSale>();
			public void run() {
			
				s = sales;
				for (Entry<String, ItemSale> entry : s.entrySet()) {
					
					if (Instant.now().getEpochSecond() >= entry.getValue().getFinishTime()) {
						sales.remove(entry.getKey());
						getConfig().set("sales." + entry.getKey(), null);
						saveConfig();
						
						getLogger().info("Sale for item '" + entry.getKey() + "' ended!");
					}
					
				}
			}
			
		}, 0, 20);
		
	}

	public static Main getInstance() {
		return instance;
	}

	public void reload() {
		saveDefaultConfig();
		reloadConfig();

		storeLink = getConfig().getString("store-link");

		premiumCurrencyName = getConfig().getString("premium-currency-name");
		premiumCurrencySymbol = getConfig().getString("premium-currency-symbol");
		premiumCurrencyColour = ChatColor.getByChar(getConfig().getString("premium-currency-colour"));
		premiumSymbolColour = ChatColor.getByChar(getConfig().getString("premium-symbol-colour"));

		cosmeticCurrencyName = getConfig().getString("cosmetic-currency-name");
		cosmeticCurrencySymbol = getConfig().getString("cosmetic-currency-symbol");
		cosmeticCurrencyColour = ChatColor.getByChar(getConfig().getString("cosmetic-currency-colour"));
		cosmeticSymbolColour = ChatColor.getByChar(getConfig().getString("cosmetic-symbol-colour"));

		storeItems.clear();
		categories.clear();
		sales.clear();

		guiName = ChatColor.translateAlternateColorCodes('&', getConfig().getString("gui-name"));

		// load items
		ConfigurationSection items = getConfig().getConfigurationSection("items");
		for (String itemName : items.getKeys(false)) {

			//getLogger().info("Loading item: " + itemName);

			ConfigurationSection item = items.getConfigurationSection(itemName);
			try {

				String permission = null;
				if (item.contains("permission")) permission = item.getString("permission");

				long buy = item.getLong("buy"); // not optional
				if (buy < 1) buy = 1;
				
				/*
				long sell = 0;
				if (item.contains("sell")) sell = item.getLong("sell");
				if (sell < 0) sell = 0;
				*/

				boolean premium = false;
				if (item.contains("premium")) premium = item.getBoolean("premium");

				boolean cosmetic = false;
				if (item.contains("cosmetic")) cosmetic = item.getBoolean("cosmetic");

				boolean canSell = false;
				if (item.contains("cansell")) canSell = item.getBoolean("cansell");

				boolean isOneTimePurchase = false;
				if(item.contains("one-time-purchase")) isOneTimePurchase = item.getBoolean("one-time-purchase");
				int maxamount = 64;
				if (item.contains("maxamount")) maxamount = item.getInt("maxamount");

				ConfigurationSection icon = item.getConfigurationSection("icon");

				ItemStack is;
				if (icon.contains("texturevalue")) {
					ItemStack temp = new ItemStack(Material.PLAYER_HEAD);
					is = SkullCreator.withBase64(temp, icon.getString("texturevalue"));
				} else {
					is = new ItemStack(Material.getMaterial(icon.getString("material")));
				}

				ItemMeta im = is.getItemMeta();

				if (icon.contains("name")) im.setDisplayName(ChatColor.translateAlternateColorCodes('&', icon.getString("name")));

				if (icon.contains("description")) {
					List<String> lore = new ArrayList<String>();
					for (String line : icon.getStringList("description")) {
						lore.add(ChatColor.translateAlternateColorCodes('&', ChatColor.RESET + line));
					}
					im.setLore(lore);
				}

				is.setItemMeta(im);

				if (icon.contains("skull") && icon.getString("material").equalsIgnoreCase("player_head")) {
					SkullMeta sm = (SkullMeta) is.getItemMeta();
					sm.setOwner(icon.getString("skull"));
					is.setItemMeta(sm);
				}

				ItemStack purchaseItem = null;
				List<String> commands = new ArrayList<String>();
				if (item.isConfigurationSection("purchaseitem")) {

					ConfigurationSection purchase = item.getConfigurationSection("purchaseitem");

					if (purchase.contains("texturevalue")) {
						ItemStack temp = new ItemStack(Material.PLAYER_HEAD);
						purchaseItem = SkullCreator.withBase64(temp, purchase.getString("texturevalue"));
					} else {
						purchaseItem = new ItemStack(Material.getMaterial(purchase.getString("material")));
					}

					im = purchaseItem.getItemMeta();

					// Have put this first to stop the name overwriting for some reason
					if (purchase.contains("skull") && purchase.getString("material").equalsIgnoreCase("player_head")) {
						SkullMeta sm = (SkullMeta) purchaseItem.getItemMeta();
						sm.setOwner(purchase.getString("skull"));
						purchaseItem.setItemMeta(sm);
					}

					if (purchase.contains("name")) im.setDisplayName(ChatColor.translateAlternateColorCodes('&', purchase.getString("name")));

					if (purchase.contains("description")) {
						List<String> lore = new ArrayList<String>();
						for (String line : purchase.getStringList("description")) {
							lore.add(ChatColor.translateAlternateColorCodes('&', ChatColor.RESET + line));
						}
						im.setLore(lore);
					}

					if (purchase.contains("enchantments")) {
						ConfigurationSection enchants = purchase.getConfigurationSection("enchantments");
						for (String enchant : enchants.getKeys(false)) {
							try {
								im.addEnchant(Enchantment.getByName(enchant), purchase.getInt("enchantments." + enchant), true);
							} catch (Exception e) {
								getLogger().severe(" Failed to load item '" + itemName + "' enchantment '" + enchant + "'!");
							}
						}
					}

					if (purchase.contains("nbt")) {
						ConfigurationSection nbt = purchase.getConfigurationSection("nbt");

						net.minecraft.server.v1_14_R1.ItemStack i = CraftItemStack.asNMSCopy(purchaseItem);
						NBTTagCompound com = new NBTTagCompound();

						for (String key : nbt.getKeys(false)) {
							com.set(key, new NBTTagString(nbt.getString(key)));
						}
						i.setTag(com);
						purchaseItem = CraftItemStack.asBukkitCopy(i);

					}

					purchaseItem.setItemMeta(im);

				}

				if (item.contains("commands")) {
					commands = item.getStringList("commands");
				}

				storeItems.put(itemName, new StoreItem(itemName, permission, is, buy, premium, cosmetic, canSell, isOneTimePurchase, maxamount, commands.toArray(new String[0]), purchaseItem));

			} catch (Exception e) {

			}

		}

		// load categories
		ConfigurationSection categories = getConfig().getConfigurationSection("categories");
		for (String categoryName : categories.getKeys(false)) {

			// getLogger().info("Loading category: " + categoryName);

			int load = 0;
			ConfigurationSection category = categories.getConfigurationSection(categoryName);
			String material = null;
			try {

				// get category info
				load = 1;
				int slot = category.getInt("slot");

				load = 2;
				material = category.getString("icon.material");
				ItemStack icon = new ItemStack(Material.getMaterial(material));

				ItemMeta meta = icon.getItemMeta();

				load = 3;
				if (category.contains("icon.name")) meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', category.getString("icon.name")));

				List<String> lore = new ArrayList<String>();
				load = 4;
				if (category.contains("icon.description")) {
					for (String line : category.getStringList("icon.description")) {
						lore.add(ChatColor.translateAlternateColorCodes('&', ChatColor.RESET + line));
					}
					meta.setLore(lore);
				}

				load = 5;
				if (category.contains("icon.glowing") && category.getBoolean("icon.glowing")) {
					meta.addEnchant(Enchantment.OXYGEN, 1, true);
					meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
				}
				meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
				meta.addItemFlags(ItemFlag.HIDE_DESTROYS);
				meta.addItemFlags(ItemFlag.HIDE_PLACED_ON);
				meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
				meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
				icon.setItemMeta(meta);

				Map<Integer, StoreItem> itemSlots = new HashMap<Integer, StoreItem>();
				try {

					ConfigurationSection i = category.getConfigurationSection("items");
					for (String item : i.getKeys(false)) {

						// TODO: Something here to do with int and location and finding the item through int
						try {
							itemSlots.put(i.getInt(item), storeItems.get(item));
						} catch (Exception e) {
							getLogger().severe(" Couldn't add item '" + item + "' to category '" + categoryName + "'!");
						}

					}

				} catch (Exception e) {
					getLogger().severe(" Category '" + categoryName + "' doesn't have an items section!");
				}

				this.categories.put(slot, new StoreCategory(categoryName, lore, slot, icon, itemSlots));

			} catch (Exception e) {

				//getLogger().severe(" Failed to load category '" + categoryName + "'!");
				switch (load) {
					case 1:
						getLogger().severe(" Failed to get slot of category '" + categoryName + "'!");
						break;
					case 2:
						getLogger().severe(" Failed to create category '" + categoryName + "' icon with material '" + material + "'!");
						break;
					case 3:
						getLogger().severe(" Failed to get category '" + categoryName + "' icon name!");
						break;
					case 4:
						getLogger().severe(" Failed to get category '" + categoryName + "' icon description!");
						break;
					case 5:
						getLogger().severe(" Failed to check if category '" + categoryName + "' icon is glowing!");
						break;
				}

			}

		}

		// load sales
		ConfigurationSection sales;
		if (getConfig().isConfigurationSection("sales")) sales = getConfig().getConfigurationSection("sales");
		else sales = getConfig().createSection("sales");

		for (String itemName : sales.getKeys(false)) {

			ConfigurationSection item = sales.getConfigurationSection(itemName);

			newSale(itemName, item.getInt("percent"), item.getLong("finish"));

			getLogger().info("Loaded sale for item '" + itemName + "'");

		}
		
	}
	
	public ServiceConnector getServiceConnector() { return sc; }
	public CrystalsAPI getCrystals() { return crystals; }
	public ArcadeCoinsAPI getArcadeCoins() { return arcadeCoins; }

	public DarkStore getDarkStore() { return darkStore; }
	
	public StoreCategory getStoreCategory(int slot) {
		if (categories.containsKey(slot)) return categories.get(slot);
		return null;
	}
	public Collection<StoreCategory> getStoreCategories() { return categories.values(); }
	
	public String getGuiName() { return guiName; }
	public String getStoreLink() { return storeLink; }
	public String getPremiumCurrencyName() { return premiumCurrencyColour + premiumCurrencyName; }
	public String getPremiumCurrencySymbol() { return premiumSymbolColour + premiumCurrencySymbol; }
	public ChatColor getPremiumCurrencyColour() { return premiumCurrencyColour; }
	public ChatColor getPremiumSymbolColour() { return premiumSymbolColour; }

	public String getCosmeticCurrencyName() { return cosmeticCurrencyColour + cosmeticCurrencyName; }
	public String getCosmeticCurrencySymbol() { return cosmeticSymbolColour + cosmeticCurrencySymbol; }
	public ChatColor getCosmeticCurrencyColour() { return cosmeticCurrencyColour; }
	public ChatColor getCosmeticSymbolColour() { return cosmeticSymbolColour; }

	public String format(String string) {
		return ChatColor.translateAlternateColorCodes('&', string);
	}

	public void addFooter(Player player, Inventory inv) {


		getServer().getScheduler().runTaskAsynchronously(this, new Runnable() {
			@Override
			public void run() {
				ItemStack emerald = new ItemStack(Material.EMERALD);
				ItemMeta meta = emerald.getItemMeta();
				meta.setDisplayName(format("&a&lBalances"));
				List<String> lore = new ArrayList<String>();
				if (Bukkit.getPluginManager().getPlugin("Essentials") != null) {
					BigDecimal m = EssentialsHook.getBalance(player);
					String cm = NumberFormat.getInstance().format(m.doubleValue());
					if (!cm.equals("-1")) {
						lore.add(format(" &6Money: " + "&f$" + "&7" + cm));
					}
				}
				final long cp = getCrystals().getCrystals(player.getName());
				final long ac = getArcadeCoins().getCoins(player.getName());
				lore.add(format(" &bCrystals: " + getPremiumCurrencySymbol() + getPremiumCurrencyColour() + cp));
				player.setMetadata("premium", new FixedMetadataValue(Main.getPlugin(Main.class), cp));
				player.setMetadata("cosmetic", new FixedMetadataValue(Main.getPlugin(Main.class), ac));
				meta.setLore(lore);
				emerald.setItemMeta(meta);
				inv.setItem(49, emerald);
			}
		});

		ItemStack exit = new ItemStack(Material.BARRIER);
		ItemMeta exitMeta = exit.getItemMeta();
		exitMeta.setDisplayName(format("&c&lExit"));
		List<String> exitLore = new ArrayList<String>();
		exitLore.clear();
		exitLore.add(format("&7Click to exit!"));
		exitMeta.setLore(exitLore);
		exit.setItemMeta(exitMeta);
		inv.setItem(45, exit);

		ItemStack star = new ItemStack(Material.PRISMARINE_CRYSTALS);
		ItemMeta starMeta = star.getItemMeta();
		starMeta.setDisplayName(format("&3&lWant more " + getPremiumCurrencyName() + "&3&l?"));
		List<String> starLore = new ArrayList<String>();
		starLore.clear();
		starLore.add(format("&fClick here!"));
		starMeta.setLore(starLore);
		star.setItemMeta(starMeta);
		inv.setItem(53, star);

		ItemStack placeholder = new ItemStack(Material.PURPLE_STAINED_GLASS_PANE); // dark grey glass panes
		ItemMeta placeholderMeta = placeholder.getItemMeta();
		placeholderMeta.setDisplayName(format("&5Indigo Games"));
		placeholder.setItemMeta(placeholderMeta);
		inv.setItem(46, placeholder);
		inv.setItem(47, placeholder);
		inv.setItem(48, placeholder);
		inv.setItem(50, placeholder);
		inv.setItem(51, placeholder);
		inv.setItem(52, placeholder);
	}

	public void updateBuyItem(Player player, StoreItem storeItem, int add, BigDecimal money, long premium, long cosmetic) {
		// purchase button, also shows price and whether player has enough money
		int quantity = 1;
		if (player.hasMetadata("buyingQuantity")) {
			quantity = player.getMetadata("buyingQuantity").get(0).asInt();
		}

		quantity += add;
		player.setMetadata("buyingQuantity", new FixedMetadataValue(this, quantity));

		ItemMeta meta;
		List<String> lore = new ArrayList<>();

		ItemStack item = storeItem.getIcon();
		item.setAmount(1);
		meta = item.getItemMeta();
		if (!meta.hasDisplayName()) {
			meta.setDisplayName(format("&e&n" + convertItemMaterial(item.getType())));
		}
		if (meta.hasLore()) {
				lore = meta.getLore();
		} else {
			lore.clear();
		}

		// Remove attributes
		meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);

		boolean sale = false;
		ItemSale itemSale = null;
		long price = storeItem.getBuyPrice();
		if ((sale = isOnSale(storeItem.getName()))) {
			itemSale = getItemSale(storeItem.getName());
			// TODO: Remove this and replace it with the right thing
			if (!meta.hasDisplayName()) {
				meta.setDisplayName(format("&e&n" + convertItemMaterial(item.getType())));
			}

			price = (long) Math.ceil((1.0 - (itemSale.getSalePercentage() / 100.0)) * price);
		}

		lore.add("");
		lore.add(format(" &6&lInformation"));
		lore.add(format("  &7Quantity: &e" + NumberFormat.getInstance().format(quantity)));
		if (storeItem.isPremium()) {
			lore.add(format("  &7Cost: &3" + premiumCurrencySymbol + "&b" + NumberFormat.getInstance().format(price * quantity)));

			if (storeItem.getBuyPrice() * quantity > premium) {
				lore.add("");
				lore.add(format("&4&l(!) &cYou need " + getPremiumCurrencySymbol() + getPremiumCurrencyColour() + NumberFormat.getInstance().format(price * quantity - premium) + " &cmore " + getPremiumCurrencyName() + "&c!"));
			} else {
				lore.add("");
				lore.add(format("&6&l(!) &eClick to buy!"));
			}
		} else if (storeItem.isCosmetic()) {
			lore.add(format("  &7Cost: " + getCosmeticSymbolColour() + getCosmeticCurrencySymbol() + getCosmeticCurrencyColour() + NumberFormat.getInstance().format(price * quantity)));

			if (storeItem.getBuyPrice() * quantity > cosmetic) {
				lore.add("");
				lore.add(format("&4&l(!) &cYou need " + getCosmeticCurrencySymbol() + getCosmeticCurrencyColour() + NumberFormat.getInstance().format(price * quantity - premium) + " &cmore " + getCosmeticCurrencyName() + "&c!"));
			} else {
				lore.add("");
				lore.add(format("&6&l(!) &eClick to buy!"));
			}
		} else {

			lore.add(format("  &7Cost: &e$" + NumberFormat.getInstance().format(price * quantity)));

			if (storeItem.getBuyPrice() * quantity > money.doubleValue()) {
				lore.add("");
				lore.add(format("&4&l(!) &cYou need &f$&7" + NumberFormat.getInstance().format(storeItem.getBuyPrice() * quantity - money.doubleValue()) + " &cmore money!"));
			} else {
				lore.add("");
				lore.add(format("&6&l(!) &eClick to buy!"));
			}
		}

		meta.setLore(lore);
		item.setItemMeta(meta);

		if (player.getOpenInventory().getTitle().contains("§8§lShop §f§l>")) {
			player.getOpenInventory().setItem(13, item);
		}
	}

	// this converts an item's material to something more readable
	// use this instead of LangUtils so we don't have to wait for it to update
	public String convertItemMaterial(Material material) {
		
		String convert = material.toString().toLowerCase().replaceAll("\\_", " ");
		String newName = "";
		
		boolean space = true;
		for (int i = 0; i < convert.length(); i++) {
			if (space) newName += Character.toUpperCase(convert.charAt(i));
			else newName += convert.charAt(i);
			space = convert.charAt(i) == ' ';
		}
		
		return newName;
	}
	
	public void newSale(String item, int percentage, long finish) {
		sales.put(item, new ItemSale(percentage, finish));
		getConfig().set("sales." + item, null);
		getConfig().createSection("sales." + item);
		getConfig().set("sales." + item + ".percent", percentage);
		getConfig().set("sales." + item + ".finish", finish);
		saveConfig();
	}
	public void stopSale(String item) {
		if (sales.containsKey(item)) {
			sales.get(item).setFinishTime(0);
		}
	}
	public Map<String, ItemSale> getSales() { return sales; }
	public boolean isOnSale(String item) { return sales.containsKey(item); }
	public ItemSale getItemSale(String item) {
		if (sales.containsKey(item)) return sales.get(item);
		return null;
	}
	
	public boolean doesItemExist(String item) { return storeItems.containsKey(item); }
	public StoreItem getStoreItem(String item) {
		if (storeItems.containsKey(item)) return storeItems.get(item);
		return null;
	}
	
    public String calculateTime(long secs) {
    	
    	secs *= 1000;
    	
        int days = (int) TimeUnit.MILLISECONDS.toDays(secs);
        long hours = TimeUnit.MILLISECONDS.toHours(secs) - (days * 24);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(secs) - (TimeUnit.MILLISECONDS.toHours(secs) * 60);
        long seconds = TimeUnit.MILLISECONDS.toSeconds(secs) - (TimeUnit.MILLISECONDS.toMinutes(secs) * 60);
        
        String time = "";
        if (days > 0) time = days + "d";
        if (hours > 0) {
            if (time.equals("")) time = hours + "h";
            else time = time + " " + hours + "h";
        }
        if (minutes > 0) {
            if (time.equals("")) time = minutes + "m";
            else time = time + " " + minutes + "m";
        }
        if (seconds > 0 || time.equals("")) {
            if (time.equals("")) time = seconds + "s";
            else time = time + " " + seconds + "s";
        }
        
        return time;
    }

	public File getPurchasesFile() { return purchasesYmlFile; }
	public YamlConfiguration getPurchasesConfig() { return purchases; }

	public File getUserSettingsFile() { return userSettingsYmlFile; }
	public YamlConfiguration getUserSettingsConfig() { return userSettings; }

	public void addNotifyPlayer(Player player) { notifyPurchasesPlayers.add(player); }
	public Set<Player> getNotifyPlayers() { return notifyPurchasesPlayers; }
	public void delNotifyPlayer(Player player) { notifyPurchasesPlayers.remove(player); }
}
