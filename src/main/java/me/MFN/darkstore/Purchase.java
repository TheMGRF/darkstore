package me.MFN.darkstore;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class Purchase {

    private String itemId, itemName;
    private int amount;
    private long time;

    public Purchase(String itemId, String itemName, int amount, long time) {
        this.itemId = itemId;
        this.itemName = itemName;
        this.amount = amount;
        this.time = time;
    }

    public String getItemId() { return itemId; }
    public String getItemName() { return itemName; }
    public int getAmount() { return amount; }
    public long getTimeInMillis() { return time; }
    public String getFormattedDate() {
        Date date = new Date(time);
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("EST"));
        return sdf.format(date);
    }

}
