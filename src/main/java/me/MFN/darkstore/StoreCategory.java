package me.MFN.darkstore;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.inventory.ItemStack;

public class StoreCategory {
	
	private String name;
	private List<String> description;
	private int guiSlot;
	private ItemStack icon;
	//private StoreItems items;
	private Map<Integer, StoreItem> items = new HashMap<Integer, StoreItem>();
	private int itemSales = 0;
	
	public StoreCategory(String name, List<String> description, int guiSlot, ItemStack icon, Map<Integer, StoreItem> items) {
		this.name = name;
		this.description = description;
		this.guiSlot = guiSlot;
		this.icon = icon;
		this.items = items;
	}
	
	public String getName() { return name; }
	public List<String> getDescription() { return description; }
	public int getGuiSlot() { return guiSlot; }
	public ItemStack getIcon() { return icon.clone(); }
	
	public StoreItem getStoreItem(int slot) {
		if (items.containsKey(slot)) {
			return items.get(slot);
		}
		return null;
	}
	public Map<Integer, StoreItem> getStoreItems() { return items; }
	
}
