package me.MFN.darkstore;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class StoreItem {

	private String name, permission;
	private ItemStack icon;
	private long buyPrice;
	private boolean premium, cosmetic, canSell, isOneTimePurchase;
	private int maxAmount;
	private ItemStack purchaseItem = null;
	private String[] commands;

	public StoreItem(String name, String permission, ItemStack icon, long buyPrice, boolean premium, boolean cosmetic, boolean canSell, boolean isOneTimePurchase, int maxAmount, String[] commands, ItemStack purchaseItem) {
		this.name = name;
		this.permission = permission;
		this.icon = icon;
		this.buyPrice = buyPrice;
		this.premium = premium;
		this.cosmetic = cosmetic;
		this.canSell = canSell;
		this.isOneTimePurchase = isOneTimePurchase;
		this.maxAmount = maxAmount;
		this.commands = commands;
		this.purchaseItem = purchaseItem;
	}

	public String getName() { return name; }
	public String getPermission() { return permission; } // can be null
	public boolean hasPermission(Player player) {
		if (permission == null || permission.equals("") || player.hasPermission(permission)) {
			return true;
		} else {
			return false;
		}
	}
	public ItemStack getIcon() { return icon.clone(); }
	public long getBuyPrice() { return buyPrice; }
	//public long getBuyPrice() { return (1 - (sale / 100)) * buyPrice; }
	public boolean isPremium() { return premium; }
	public boolean isCosmetic() { return cosmetic; }
	public boolean canSell() { return canSell; }
	public boolean isOneTimePurchase() { return isOneTimePurchase; }
	public int getMaxAmount() { return maxAmount; }
	public String[] getCommands() { return commands.clone(); }
	public String[] getCommands(Player player, int amount) {
		String[] c = commands.clone(); // .clone() fixes the first player buying it getting all the items from then onwards (clones it not links it)
		String username = player.getName();
		String uuid = player.getUniqueId().toString();
		for (int i = 0; i < c.length; i++) {
			c[i] = c[i]
					.replaceAll("<username>", username)
					.replaceAll("<uuid>", uuid)
					.replaceAll("<amount>", amount + "");
		}
		return c;
	}
	public ItemStack getPurchaseItem() {
		if (purchaseItem != null) return purchaseItem.clone();
		return null;
	}

}
