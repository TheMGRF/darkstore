package me.MFN.darkstore.commands;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.NumberFormat;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;

import com.earth2me.essentials.api.UserDoesNotExistException;

import me.MFN.darkstore.Main;
import net.ess3.api.Economy;
import net.md_5.bungee.api.ChatColor;

public class BalanceCmd implements CommandExecutor {
	
	private Main main;
	private FixedMetadataValue meta;
	
	public BalanceCmd(Main main) {
		this.main = main;
		meta = new FixedMetadataValue(main, 0);
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		
		if (!(sender instanceof Player)) {
			sender.sendMessage("Cannot do this from the console");
			return true;
		}
		
		Player p = (Player) sender;
		if (p.hasMetadata("checkingbalance")) return true;
		
		Player balanceplayer = null;
		if (args.length > 0 && p.hasPermission("balance.checkothers")) {
			
			// check if player is online
			balanceplayer = main.getServer().getPlayer(args[0]);
			if (balanceplayer == null) {
				
				p.sendMessage("Player '" + args[0] + "' is offline!");
				return true;
				
			}
			
		} else balanceplayer = p;
		
		p.setMetadata("checkingbalance", meta);
		final String playerUsername = p.getName();
		final String username = balanceplayer.getName();
		final String uuid = balanceplayer.getUniqueId().toString();
		
		String moneyAmount = "0";
		try {
			moneyAmount = NumberFormat.getInstance().format(Economy.getMoneyExact(username).doubleValue());
		} catch (UserDoesNotExistException e1) {
		}
		
		final String mA = moneyAmount;
		main.getServer().getScheduler().runTaskAsynchronously(main, new Runnable() { public void run() {
			
			try (Connection con = main.getServiceConnector().getSql().getConnection()) {
				
				long crystals = 0;
				
				PreparedStatement stmt = con.prepareStatement("SELECT crystals FROM crystals WHERE uuid='" + uuid + "' LIMIT 1");
				ResultSet rs = stmt.executeQuery();
				if (rs.next()) {
					crystals = rs.getLong(1);
				}
				rs.close();
				stmt.close();
				
				long c = crystals;
				main.getServer().getScheduler().scheduleSyncDelayedTask(main, new Runnable() { public void run() {

					p.removeMetadata("checkingbalance", main);
					
					String player = "";
					if (!playerUsername.equals(username)) player = ChatColor.GOLD + username + "'s ";
					sender.sendMessage(ChatColor.YELLOW + "● " + player + ChatColor.GOLD + "Money: " + ChatColor.WHITE + "$" + ChatColor.GRAY + mA);
					sender.sendMessage(ChatColor.YELLOW + "● " + player + main.getPremiumCurrencyName() + ": " + main.getPremiumCurrencySymbol() + main.getPremiumCurrencyColour() + c);
					
				}});
				
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
		}});
		
		return true;
	}
	
}
