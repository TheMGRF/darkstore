package me.MFN.darkstore.commands;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

import me.MFN.darkstore.Main;
import me.MFN.darkstore.Purchase;
import net.md_5.bungee.api.ChatColor;

public class PurchasesCmd implements CommandExecutor {

    private Main main;
    private int resultsPerPage = 10;

    public PurchasesCmd(Main main) {
        this.main = main;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (!sender.hasPermission("darkstore.purchases")) {
            sender.sendMessage(ChatColor.RED + "No permission");
            return true;
        }

        /*
         * args:
         *  username
         *  page
         */


        String username = null;
        if (args.length > 0) username = args[0];
        else if (sender instanceof Player) username = ((Player) sender).getName();

        int page = 1;
        if (args.length > 1) {
            try {
                page = Integer.parseInt(args[1]);
            } catch (NumberFormatException e) {}
        }
        if (page < 1) page = 1;

        if (username == null) {

            sender.sendMessage(ChatColor.GOLD + "/purchases " + ChatColor.WHITE + " <username>" + ChatColor.GRAY + " [page]");

            return true;
        }

        // check if user exists in purchases.yml's users section
        ConfigurationSection userSection = main.getPurchasesConfig().getConfigurationSection("users");
        if (!userSection.isConfigurationSection(username.toLowerCase())) {
            sender.sendMessage("User '" + username + "' has never bought anything!");
            return true;
        }
        username = userSection.getString(username.toLowerCase() + ".username");
        String uuid = userSection.getString(username.toLowerCase() + ".uuid");

        // get list of player's purchases
        List<Purchase> purchases = new ArrayList<Purchase>();
        ConfigurationSection playerSection = main.getPurchasesConfig().getConfigurationSection("purchases").getConfigurationSection(uuid).getConfigurationSection("purchases");
        for (String timeString : playerSection.getKeys(false)) {

            long time = Long.parseLong(timeString);

            String itemId = playerSection.getString(timeString + ".itemId");
            String itemName = playerSection.getString(timeString + ".itemName");
            int amount = playerSection.getInt(timeString + ".amount");

            purchases.add(new Purchase(itemId, itemName, amount, time));
        }

        // display up to 'resultsPerPage' results per page
        int start = (page - 1) * resultsPerPage;
        int pages = (int) Math.ceil((double) purchases.size() / (double) resultsPerPage); // cast to double or else it doesn't divide properly
        sender.sendMessage(ChatColor.GOLD + username + "'s purchases: " + ChatColor.GRAY + "(Page " + ChatColor.WHITE + page + ChatColor.GRAY + "/" + pages + ")");
        for (int i = 0; i < resultsPerPage; i++) {
            if (purchases.size() - 1 < start + i) break;

            Purchase p = purchases.get(start + i);
            sender.sendMessage(ChatColor.GOLD + "" + (start + i + 1) + ": " + ChatColor.GRAY + p.getAmount() + "x " + ChatColor.WHITE + p.getItemName() + ChatColor.YELLOW + " " + p.getFormattedDate() + "");
        }

        return true;
    }

}
