package me.MFN.darkstore.commands;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import me.MFN.darkstore.ItemSale;
import me.MFN.darkstore.Main;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.TextComponent;

public class SalesCmd implements CommandExecutor {
	
	private Main main;
	
	public SalesCmd(Main main) {
		this.main = main;
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if (!sender.hasPermission("darkstore.sales")) {
			sender.sendMessage(ChatColor.RED + "No permission");
			return true;
		}
		
		if (args.length > 0) {
			switch (args[0]) {
			case "create":
				
				/*
				 * [0] 'create'
				 * [1] item name from config
				 * [2] percentage
				 * [3] duration in seconds
				 */
				
				if (args.length > 3) {
					
					if (!main.doesItemExist(args[1])) {
						sender.sendMessage("Item '" + args[1] + "' doesn't exist in the config!");
						return true;
					}
					
					if (main.isOnSale(args[1])) {
						sender.sendMessage("Item '" + args[1] + "' is already on sale!");
						return true;
					}
					
					if (!main.getStoreItem(args[1]).isPremium()) {
						sender.sendMessage("Only premium items can be put on sale!");
						return true;
					}
					
					int percentage = 0;
					try {
						percentage = Integer.parseInt(args[2]);
					} catch (NumberFormatException e) {
						sender.sendMessage("Percentage must be an integer, not '" + args[2] + "'");
						return true;
					}
					if (percentage <= 0) {
						sender.sendMessage("Percentage must be more than 0!");
						return true;
					}
					if (percentage >= 100) {
						sender.sendMessage("Percentage must be less than 100!");
						return true;
					}
					
					long duration = 0;
					try {
						duration = Long.parseLong(args[3]);
					} catch (NumberFormatException e) {
						sender.sendMessage("Duration must be an integer, not '" + args[3] + "'");
						return true;
					}
					if (duration <= 0) {
						sender.sendMessage("Duration must be more than 0 seconds!");
						return true;
					}
					
					long finish = Instant.now().getEpochSecond() + duration;
					
					main.newSale(args[1], percentage, finish);
					
					sender.sendMessage("Item '" + args[1] + "' is now on sale for " + percentage + "% until " + main.calculateTime(finish - Instant.now().getEpochSecond()) + "! (old price: " + main.getStoreItem(args[1]).getBuyPrice() + ", new price: " + ((long) Math.ceil((1.0 - (percentage / 100.0)) * main.getStoreItem(args[1]).getBuyPrice())) + ")");
					
				} else {
					sender.sendMessage("/sales create <" + ChatColor.GRAY + "item name in config" + ChatColor.WHITE + "> <" + ChatColor.GRAY + "percent" + ChatColor.WHITE + "> <" + ChatColor.GRAY + "duration in seconds" + ChatColor.WHITE + ">");
				}
				
				break;
			case "delete":
				
				if (args.length > 1) {
					
					if (!main.doesItemExist(args[1])) {
						sender.sendMessage("Item '" + args[1] + "' doesn't exist in the config!");
						return true;
					}
					
					if (!main.isOnSale(args[1])) {
						sender.sendMessage("Item '" + args[1] + "' is not on sale!");
						return true;
					}
					
					main.stopSale(args[1]);
					
					sender.sendMessage("item '" + args[1] + "' sale ended! (price: " + main.getStoreItem(args[1]).getBuyPrice() + ")");
					
				} else {
					sender.sendMessage("/sales delete <" + ChatColor.GRAY + "item name in config" + ChatColor.WHITE + ">");
				}
				
				break;
			default:
				sender.sendMessage("/sales");
				sender.sendMessage("/sales create <" + ChatColor.GRAY + "item name in config" + ChatColor.WHITE + "> <" + ChatColor.GRAY + "percent" + ChatColor.WHITE + "> <" + ChatColor.GRAY + "duration in seconds" + ChatColor.WHITE + ">");
				sender.sendMessage("/sales delete <" + ChatColor.GRAY + "item name in config" + ChatColor.WHITE + ">");
				break;
			}
		} else {
			
			// list active sales
			List<TextComponent> sales = new ArrayList<TextComponent>();
			sales.add(new TextComponent(ChatColor.GOLD + "Active sales:"));
			
			for (Entry<String, ItemSale> entry : main.getSales().entrySet()) {
				sales.add(new TextComponent(" " + entry.getKey() + ChatColor.GRAY + ", " + ChatColor.WHITE + entry.getValue().getSalePercentage() + "%" + ChatColor.GRAY + ", " + ChatColor.WHITE + "ends in " + main.calculateTime(entry.getValue().getFinishTime() - Instant.now().getEpochSecond())));
			}
			
			sender.spigot().sendMessage(sales.toArray(new TextComponent[0]));
			
		}
		
		return true;
	}
	
}
