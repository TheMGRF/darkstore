package me.MFN.darkstore.commands;

import java.math.BigDecimal;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.earth2me.essentials.api.NoLoanPermittedException;
import com.earth2me.essentials.api.UserDoesNotExistException;

import me.MFN.darkstore.Main;
import net.ess3.api.Economy;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.TextComponent;

public class SellCmd implements CommandExecutor {
	
	private Main plugin;
	
	public SellCmd(Main plugin) {
		this.plugin = plugin;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if (!(sender instanceof Player)) {
			sender.sendMessage("derp");
			return true;
		}
		
		
		Player p = (Player) sender;
		/*
		if (p.hasMetadata("selldisable")) {
			p.sendMessage(ChatColor.RED + "�? One moment, your previous item is still selling!");
			return true;
		}
		*/
		
		final ItemStack heldItem = p.getInventory().getItemInMainHand().clone();
		if (heldItem == null || heldItem.getType() == Material.AIR) {
			p.spigot().sendMessage(new TextComponent(ChatColor.YELLOW + "● " + ChatColor.GOLD + "Hold an item to sell it!"),
					new TextComponent("\n"), new TextComponent(ChatColor.WHITE + " ► " + ChatColor.GRAY + "/sell [" + ChatColor.WHITE + "amount" + ChatColor.GRAY + "]"));
			return true;
		}
		
		if (!plugin.getConfig().contains("sell." + heldItem.getType().toString())) {
			p.spigot().sendMessage(new TextComponent(ChatColor.RED + "● This item cannot be sold!"));
			return true;
		}

		long amount = heldItem.getAmount();
		if (args.length > 0) {
			try {
				amount = Long.parseLong(args[0]);
			} catch (NumberFormatException e) {
			} finally {
				if (amount <= 0) amount = 1;
				else if (amount > heldItem.getMaxStackSize()) amount = heldItem.getMaxStackSize();
				if (amount > heldItem.getAmount()) amount = heldItem.getAmount();
			}
		}
		
		BigDecimal price = new BigDecimal(plugin.getConfig().getString("sell." + heldItem.getType().toString()));
		price = price.multiply(new BigDecimal(amount));
		//BigDecimal sellPrice = price * amount;
		//final String uuid = p.getUniqueId().toString();
		
		int newAmount = heldItem.getAmount() - (int) amount;
		p.getInventory().getItemInMainHand().setAmount(newAmount);
		
		try {
			Economy.add(p.getName(), price);
		} catch (NoLoanPermittedException | ArithmeticException | UserDoesNotExistException e) {
			e.printStackTrace();
		}
		
		p.spigot().sendMessage(new TextComponent(ChatColor.YELLOW + "● " + ChatColor.GOLD + "Sold " + ChatColor.GRAY + amount + "x " + plugin.convertItemMaterial(heldItem.getType()) + ChatColor.GOLD + " for " + ChatColor.WHITE + "$" + ChatColor.GRAY + price.toPlainString() + ChatColor.GOLD + "!"));
		
		/*
		main.getServer().getScheduler().runTaskAsynchronously(main, new Runnable() { public void run() {
			
			try (Connection con = main.getServiceConnector().getSql().getConnection()) {
				
				PreparedStatement stmt = con.prepareStatement("UPDATE players SET money=money+" + sellPrice + " WHERE uuid='" + uuid + "' LIMIT 1");
				stmt.executeUpdate();
				stmt.close();
				
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			TextComponent text = new TextComponent("");
			if (boost > 1) {
				text = new TextComponent("\n" + ChatColor.WHITE + " ► " + ChatColor.GRAY + "Earned " + ChatColor.WHITE + "$" + (sellPrice - (price * amount)) + " extra " + ChatColor.GRAY + "with " + ChatColor.WHITE + boost + "x Economy boost!");
			}
			
			final TextComponent t = text;
			main.getServer().getScheduler().scheduleSyncDelayedTask(main, new Runnable() { public void run() {
				
				p.spigot().sendMessage(new TextComponent(ChatColor.YELLOW + "�? " + ChatColor.GOLD + "Sold " + ChatColor.GRAY + amount + "x " + LanguageHelper.getItemDisplayName(heldItem, p) + ChatColor.GOLD + " for " + ChatColor.WHITE + "$" + ChatColor.GRAY + sellPrice + ChatColor.GOLD + "!"), t);
				p.removeMetadata("selldisable", main);
				
			}});
			
		}});
		*/
		
		return true;
	}
	
}
