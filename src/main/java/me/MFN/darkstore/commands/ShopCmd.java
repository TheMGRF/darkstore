package me.MFN.darkstore.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.MFN.darkstore.Main;

public class ShopCmd implements CommandExecutor {
	
	private Main main;
	
	public ShopCmd(Main main) {
		this.main = main;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String args[]) {
		
		if (!(sender instanceof Player)) {
			main.reload();
			sender.sendMessage("Store config reloaded!");
			return true;
		}
		
		Player p = (Player) sender;
		if (args.length > 0 && p.hasPermission("store.reload")) {
			main.reload();
			p.sendMessage("Store config reloaded!");
		} else {
			main.getDarkStore().openStoreGui(p);
		}
		
		return true;
	}
	
}
