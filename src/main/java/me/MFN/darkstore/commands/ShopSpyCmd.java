package me.MFN.darkstore.commands;

import java.io.IOException;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

import me.MFN.darkstore.Main;
import net.md_5.bungee.api.ChatColor;

public class ShopSpyCmd implements CommandExecutor {

    private Main main;

    public ShopSpyCmd(Main main) {
        this.main = main;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (!sender.hasPermission("darkstore.shopspy")) {
            sender.sendMessage(ChatColor.RED + "No permission");
            return true;
        }
        Player player = (Player) sender;
        String uuid = player.getUniqueId().toString();

        ConfigurationSection users = main.getUserSettingsConfig().getConfigurationSection("users");
        ConfigurationSection userUuid;
        if (!users.isConfigurationSection(uuid)) {
            userUuid = users.createSection(uuid);
            userUuid.set("notify", false);
        }
        else userUuid = users.getConfigurationSection(uuid);

        // if the user is already being notified, disable it
        if (userUuid.getBoolean("notify")) {

            userUuid.set("notify", false);
            main.delNotifyPlayer(player);
            sender.sendMessage(ChatColor.GOLD + "Shop purchase notifications " + ChatColor.RED + "disabled");

            // otherwise enabled it
        } else {

            userUuid.set("notify", true);
            main.addNotifyPlayer(player);
            sender.sendMessage(ChatColor.GOLD + "Shop purchase notifications " + ChatColor.GREEN + "enabled");

        }

        try {
            main.getUserSettingsConfig().save(main.getUserSettingsFile());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return true;
    }

}
