package me.MFN.darkstore.commands;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import me.MFN.darkstore.Main;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;

public class StoreCmd implements CommandExecutor {
	
	private TextComponent[] storeMsgs;
	
	public StoreCmd(Main main) {
		
		TextComponent store = new TextComponent("store.indigo.games");
		store.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.GOLD + "Click to visit the Store!").create()));
		store.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "http://store.indigo.games"));
		
		List<TextComponent> s = new ArrayList<TextComponent>();
		s.add(new TextComponent("\n"));
		s.add(new TextComponent(ChatColor.YELLOW + "● " + ChatColor.GOLD + "Get " + main.getPremiumCurrencySymbol() + main.getPremiumCurrencyName() + ChatColor.GOLD + " at: "));
		s.add(new TextComponent(store));
		s.add(new TextComponent("\n"));
		
		storeMsgs = s.toArray(new TextComponent[0]);
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		sender.spigot().sendMessage(storeMsgs);
		
		return true;
	}
	
}
