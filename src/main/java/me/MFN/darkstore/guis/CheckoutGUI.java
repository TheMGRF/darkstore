package me.MFN.darkstore.guis;

import me.MFN.darkstore.EssentialsHook;
import me.MFN.darkstore.Main;
import me.MFN.darkstore.StoreItem;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.metadata.FixedMetadataValue;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

public class CheckoutGUI {

    private Main main;

    public CheckoutGUI(Main main) {
        this.main = main;
    }

    public void open(Player player, StoreItem item) {
        player.playSound(player.getLocation(), Sound.UI_BUTTON_CLICK, 1, 1);
        player.setMetadata("buyingQuantity", new FixedMetadataValue(main, 1));

        // check if player can afford the item
        BigDecimal money;
        if (Bukkit.getPluginManager().getPlugin("Essentials") != null) {
            money = EssentialsHook.getBalance(player);
        } else {
            money = BigDecimal.valueOf(-1);
        }

        long premium = player.getMetadata("premium").get(0).asLong();
        long cosmetic = player.getMetadata("cosmetic").get(0).asLong();

        Inventory inv = main.getServer().createInventory(null, 36, main.getGuiName() + " §f§l> §e§l" + main.convertItemMaterial(item.getIcon().getType()));

        ItemStack back = new ItemStack(Material.ARROW);
        ItemMeta meta = back.getItemMeta();
        meta.setDisplayName(main.format("&7Back"));
        back.setItemMeta(meta);
        inv.setItem(27, back);

        ItemStack emerald = new ItemStack(Material.EMERALD);
        meta = emerald.getItemMeta();
        meta.setDisplayName(main.format("&a&lBalances"));
        List<String> lore = new ArrayList<String>();
        lore.add(ChatColor.WHITE + "$" + ChatColor.GRAY + NumberFormat.getInstance().format(money.doubleValue()));
        lore.add(main.getPremiumCurrencySymbol() + main.getPremiumCurrencyColour() + NumberFormat.getInstance().format(premium));
        meta.setLore(lore);
        emerald.setItemMeta(meta);
        inv.setItem(31, emerald);

        ItemStack star = new ItemStack(Material.PRISMARINE_CRYSTALS);
        meta = star.getItemMeta();
        meta.setDisplayName(main.format("&3&lWant more " + main.getPremiumCurrencySymbol() + main.getPremiumCurrencyName() + "&3&l?"));
        lore.clear();
        lore.add(ChatColor.WHITE + "Click here!");
        meta.setLore(lore);
        star.setItemMeta(meta);
        inv.setItem(35, star);

        ItemStack placeholder = new ItemStack(Material.LIME_STAINED_GLASS_PANE);
        meta = placeholder.getItemMeta();
        meta.setDisplayName(ChatColor.GREEN + "Indigo Games");
        placeholder.setItemMeta(meta);
        inv.setItem(28, placeholder);
        inv.setItem(29, placeholder);
        inv.setItem(30, placeholder);

        inv.setItem(32, placeholder);
        inv.setItem(33, placeholder);
        inv.setItem(34, placeholder);

        if(!item.isOneTimePurchase()) {
            ItemStack increase = new ItemStack(Material.POTION); // full glass bottle
            meta = increase.getItemMeta();
            meta.setDisplayName(main.format("&a&lAdd 1"));
            meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
            increase.setItemMeta(meta);
            inv.setItem(15, increase);

            ItemStack add16 = new ItemStack(Material.POTION);
            meta = add16.getItemMeta();
            meta.setDisplayName(main.format("&a&lAdd 16"));
            meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
            add16.setItemMeta(meta);
            inv.setItem(16, add16);

            ItemStack add64 = new ItemStack(Material.POTION);
            meta = add64.getItemMeta();
            meta.setDisplayName(main.format("&a&lAdd 64"));
            meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
            add64.setItemMeta(meta);
            inv.setItem(17, add64);

            ItemStack decrease = new ItemStack(Material.GLASS_BOTTLE); // empty glass bottle
            meta = decrease.getItemMeta();
            meta.setDisplayName(main.format("&c&lRemove 1"));
            decrease.setItemMeta(meta);
            inv.setItem(11, decrease);

            ItemStack remove16 = new ItemStack(Material.GLASS_BOTTLE);
            meta = remove16.getItemMeta();
            meta.setDisplayName(main.format("&c&lRemove 16"));
            remove16.setItemMeta(meta);
            inv.setItem(10, remove16);

            ItemStack remove64 = new ItemStack(Material.GLASS_BOTTLE);
            meta = remove64.getItemMeta();
            meta.setDisplayName(main.format("&c&lRemove 64"));
            remove64.setItemMeta(meta);
            inv.setItem(9, remove64);
        }

        Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
            @Override
            public void run() {
                main.updateBuyItem(player, item, 0, money, premium, cosmetic);
            }
        });

        player.openInventory(inv);
    }

}
