package me.MFN.darkstore.listeners;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Map;
import java.util.Map.Entry;

import me.MFN.darkstore.EssentialsHook;
import me.MFN.darkstore.guis.CheckoutGUI;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;

import me.MFN.darkstore.Main;
import me.MFN.darkstore.StoreCategory;
import me.MFN.darkstore.StoreItem;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;

public class InventoryClickListener implements Listener {

    private Main main;
    private CheckoutGUI checkoutGUI;

    private TextComponent webStore;
    private ConfigurationSection purchaseSection, userSection;

    public InventoryClickListener(Main main, CheckoutGUI checkoutGUI, YamlConfiguration purchases) {
        this.main = main;
        this.checkoutGUI = checkoutGUI;

        if (!purchases.isConfigurationSection("purchases")) purchases.createSection("purchases");
        purchaseSection = purchases.getConfigurationSection("purchases");
        if (!purchases.isConfigurationSection("users")) purchases.createSection("users");
        userSection = purchases.getConfigurationSection("users");

        webStore = new TextComponent(ChatColor.WHITE + main.getStoreLink());
        webStore.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.DARK_AQUA + "Click to get more " + main.getPremiumCurrencySymbol() + main.getPremiumCurrencyName() + ChatColor.DARK_AQUA + "!").create()));
        webStore.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, main.getStoreLink()));
    }

    @EventHandler
    public void click(InventoryClickEvent e) {

        final int slot = e.getSlot();

        //if (slot != -999 && e.getInventory().getName().equals(main.getGuiName())) {
        Player player = (Player) e.getWhoClicked();
        Inventory inv = e.getClickedInventory();

        if (inv != null) {
            if (slot != -999 && e.getView().getTitle().contains("§8§lShop")) {
                e.setCancelled(true);

                String username = player.getName();

                BigDecimal money;
                if (Bukkit.getPluginManager().getPlugin("Essentials") != null) {
                    money = EssentialsHook.getBalance(player);
                } else {
                    money = BigDecimal.valueOf(-1);
                }
                long premium = player.getMetadata("premium").get(0).asLong();
                long cosmetic = player.getMetadata("cosmetic").get(0).asLong();

                if (e.getView().getTitle().equals("§8§lShop")) {
                    if (slot == 49 || slot == 53) {

                        // give store link to player
                        player.closeInventory();
                        player.spigot().sendMessage(new TextComponent("\n"), new TextComponent(ChatColor.YELLOW + "● " + ChatColor.GOLD + "Want more " + main.getPremiumCurrencySymbol() + main.getPremiumCurrencyName() + ChatColor.GOLD + "?"),
                                new TextComponent("\n"), new TextComponent(ChatColor.WHITE + " ► " + ChatColor.GRAY + "Get them at "), webStore);
                    } else if (slot == 45) {
                        player.playSound(player.getLocation(), Sound.UI_BUTTON_CLICK, 1, 2);
                        player.closeInventory();
                    } else {
                        main.getDarkStore().openCategoryGui(player, main.getStoreCategory(slot), premium, cosmetic);
                    }
                } else if (e.getView().getTitle().contains(main.getGuiName() + " §f§l> §6§l")) {
                    if (slot == 49 || slot == 50) {
                        // give store link to player
                        player.playSound(player.getLocation(), Sound.UI_BUTTON_CLICK, 1, 1);
                        player.closeInventory();
                        player.spigot().sendMessage(new TextComponent("\n"), new TextComponent(ChatColor.YELLOW + "● " + ChatColor.GOLD + "Want more " + main.getPremiumCurrencySymbol() + main.getPremiumCurrencyName() + ChatColor.GOLD + "?"),
                                new TextComponent("\n"), new TextComponent(ChatColor.WHITE + " ► " + ChatColor.GRAY + "Get them at "), webStore);
                    } else if (slot == 45) {
                        main.getDarkStore().openStoreGui(player);
                    } else {
                        StoreCategory category = main.getStoreCategory(player.getMetadata("storecategory").get(0).asInt());
                        StoreItem item = category.getStoreItem(slot);
                        if (item != null) {

                            long price = item.getBuyPrice();
                            if (main.isOnSale(item.getName())) {
                                price = (long) Math.ceil((1.0 - (main.getItemSale(item.getName()).getSalePercentage() / 100.0)) * price);
                            }

                            // buy item
                            if (e.getClick() == ClickType.LEFT || e.getClick() == ClickType.SHIFT_LEFT) {
                                if (item.hasPermission(player)) {
                                    if (item.isPremium()) {
                                        if (price > premium) {
                                            // close player's inventory and give them a webstore link
                                            player.playSound(player.getLocation(), Sound.UI_BUTTON_CLICK, 1, 1);
                                            player.closeInventory();
                                            player.spigot().sendMessage(new TextComponent("\n"), new TextComponent(ChatColor.RED + "● You need more " + main.getPremiumCurrencySymbol() + main.getPremiumCurrencyName() + ChatColor.RED + "!"),
                                                    new TextComponent("\n"), new TextComponent(ChatColor.WHITE + " ► " + ChatColor.GRAY + "Get them at "), webStore);

                                            return;
                                        }
                                    } else if (item.isCosmetic()) {
                                        if (price > cosmetic) {
                                            // close player's inventory and give them a webstore link
                                            player.playSound(player.getLocation(), Sound.UI_BUTTON_CLICK, 1, 1);
                                            player.closeInventory();
                                            player.spigot().sendMessage(new TextComponent("\n"), new TextComponent(ChatColor.RED + "● You need more " + main.getCosmeticCurrencySymbol() + main.getCosmeticCurrencyName() + ChatColor.RED + "!"),
                                                    new TextComponent("\n"), new TextComponent(ChatColor.WHITE + " ► " + ChatColor.GRAY + "Get them at "), webStore);

                                            return;
                                        }
                                    } else {
                                        if (price > money.doubleValue()) {
                                            player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_BASS, 1, 1);
                                            player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_SNARE, 1, 0.1f);
                                            return;
                                        }
                                    }
                                } else {
                                    player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_BASS, 1, 1);
                                    return;
                                }

                                //main.getDarkStore().openBuyGui(p, item, slot, 1);
                                checkoutGUI.open(player, item);
                                // sell item
                            } else {
                                try {
                                    // check if item is sellable
                                    if (item.getPurchaseItem().getType() == Material.SPAWNER) return;

                                    BigDecimal sellPrice = BigDecimal.valueOf(0);
                                    if (EssentialsHook.getEss() != null) {
                                        sellPrice = EssentialsHook.getItemWorth(item.getPurchaseItem());
                                    }

                                    if (sellPrice == null) return;

                                    // check if player has any of this item in their inventory
                                    if (!player.getInventory().containsAtLeast(item.getPurchaseItem(), 1)) return;

                                    // get how many of this item there are in player's inventory and remove them at the same time (so it's not looping the inventory twice)
                                    ItemStack purchaseItem = item.getPurchaseItem();
                                    int amount = 0;
                                    int loopSlot = 0;
                                    for (ItemStack is : player.getInventory().getContents()) {
                                        if (is != null && is.isSimilar(purchaseItem)) {
                                            amount += is.getAmount();
                                            player.getInventory().clear(loopSlot);
                                        }
                                        loopSlot++;
                                    }

                                    // get total sell price
                                    BigDecimal totalSellPrice = sellPrice.multiply(new BigDecimal(amount));

                                    // add money to player's balance
                                    EssentialsHook.giveMoney(player, totalSellPrice);

                                    String name = item.getName();
                                    if (name == null) name = main.convertItemMaterial(item.getPurchaseItem().getType());

                                    player.sendMessage(main.format("&6Sold &7" + NumberFormat.getInstance().format(amount) + "x &f" + name + " &6for &f$&7" + totalSellPrice.toPlainString()));
                                    player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_BELL, 1, 1);
                                    player.closeInventory();
                                } catch (NullPointerException error) {
                                }
                            }

                            if (item.hasPermission(player)) {
                                if (item.isPremium()) {
                                    if (item.getBuyPrice() > premium) {

                                        // close player's inventory and give them a webstore link
                                        player.playSound(player.getLocation(), Sound.UI_BUTTON_CLICK, 1, 1);
                                        player.closeInventory();
                                        player.spigot().sendMessage(new TextComponent("\n"), new TextComponent(ChatColor.RED + "● You need more " + main.getPremiumCurrencySymbol() + main.getPremiumCurrencyName() + ChatColor.RED + "!"),
                                                new TextComponent("\n"), new TextComponent(ChatColor.WHITE + " ► " + ChatColor.GRAY + "Get them at "), webStore);

                                        return;
                                    }
                                } else if (item.isCosmetic()) {
                                    if (item.getBuyPrice() > cosmetic) {

                                        // close player's inventory and give them a webstore link
                                        player.playSound(player.getLocation(), Sound.UI_BUTTON_CLICK, 1, 1);
                                        player.closeInventory();
                                        player.spigot().sendMessage(new TextComponent("\n"), new TextComponent(ChatColor.RED + "● You need more " + main.getCosmeticCurrencySymbol() + main.getCosmeticCurrencyName() + ChatColor.RED + "!"),
                                                new TextComponent("\n"), new TextComponent(ChatColor.WHITE + " ► " + ChatColor.GRAY + "Get them at "), webStore);

                                        return;
                                    }
                                } else {
                                    if (item.getBuyPrice() > money.doubleValue()) {
                                        player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_BASS, 1, 1);
                                        player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_SNARE, 1, 0.1f);
                                        return;
                                    }
                                }
                            } else {
                                player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_BASS, 1, 1);
                                return;
                            }

                            player.setMetadata("storemenu", new FixedMetadataValue(main, 2));
                            player.setMetadata("storeitem", new FixedMetadataValue(main, slot));
                        }
                    }
                } else if (e.getView().getTitle().contains(main.getGuiName() + " §f§l> §e§l")) {
                    player.playSound(player.getLocation(), Sound.UI_BUTTON_CLICK, 1, 1);

                    int quantity = 0;

                    if (slot == 31 || slot == 35) {
                        // give store link to player
                        player.closeInventory();
                        player.playSound(player.getLocation(), Sound.UI_BUTTON_CLICK, 1, 1);
                        player.spigot().sendMessage(new TextComponent("\n"), new TextComponent(ChatColor.YELLOW + "● " + ChatColor.GOLD + "Want more " + main.getPremiumCurrencySymbol() + main.getPremiumCurrencyName() + ChatColor.GOLD + "?"),
                                new TextComponent("\n"), new TextComponent(ChatColor.WHITE + " ► " + ChatColor.GRAY + "Get them at "), webStore);
                        return;

                    } else if (slot == 27) {
                        main.getDarkStore().openStoreGui(player);
                        return;
                    } else if (slot == 13) {
                        // purchase item if player has enough money
                        BigDecimal m = BigDecimal.valueOf(-1);
                        if (Bukkit.getPluginManager().getPlugin("Essentials") != null) {
                            m = EssentialsHook.getBalance(player);
                        }

                        StoreCategory category = main.getStoreCategory(player.getMetadata("storecategory").get(0).asInt());
                        final StoreItem item = category.getStoreItem(player.getMetadata("storeitem").get(0).asInt());

                        // check if player can still afford it
                        int amount = player.getMetadata("buyingQuantity").get(0).asInt();
                        long price = item.getBuyPrice() * amount;
                        if ((item.isPremium() && premium < price) || (!item.isPremium() && m.doubleValue() < price)) {
                            return;
                        }
                        if ((item.isCosmetic() && cosmetic < price) || (!item.isCosmetic() && m.doubleValue() < price)) {
                            return;
                        }

                        main.getLogger().info(player.getName() + " bought " + item.getName() + " (" + category.getName() + ")");

                        // check if player has enough inventory space (if the purchase item isn't null)
                        if (item.getPurchaseItem() != null) {
                            ItemStack purchaseItem = item.getPurchaseItem();
                            // TODO: purchaseItem.getMaxStackSize()
                            purchaseItem.setAmount(amount);

                            Map<Integer, ItemStack> failed = player.getInventory().addItem(purchaseItem);
                            if (!failed.isEmpty()) {
                                // drop failed items on the ground
                                for (Entry<Integer, ItemStack> entry : failed.entrySet()) {
                                    ItemStack drop = entry.getValue().clone();
                                    int stackAmount = drop.getAmount() / drop.getMaxStackSize();
                                    int leftover = drop.getAmount() % drop.getMaxStackSize();
                                    drop.setAmount(drop.getMaxStackSize());
                                    // drop full stacks of the failed drop
                                    for (int i = 0; i < stackAmount; i++) {
                                        player.getWorld().dropItem(player.getLocation(), drop);
                                    }
                                    // drop the leftover after full stacks of the failed drop
                                    drop.setAmount(leftover);
                                    player.getWorld().dropItem(player.getLocation(), drop);
                                }
                                player.closeInventory();
                                player.sendMessage(ChatColor.RED + "● Your inventory was full, dropping items on floor!");
                                player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_BASS, 1, 1);
                                return;
                            }
                        }

                        // execute commands
                        if (item.getCommands().length > 0) {
                            for (String command : item.getCommands(player, amount)) {
                                for (int i = 0; i < amount; i++) {
                                    main.getLogger().info("> (" + (i + 1) + ") " + command);
                                    main.getServer().dispatchCommand(main.getServer().getConsoleSender(), command);
                                }
                            }
                        }

                        String n;
                        if (item.getIcon().getItemMeta().hasDisplayName())
                            n = item.getIcon().getItemMeta().getDisplayName();
                        else n = main.convertItemMaterial(item.getIcon().getType());
                        String name = n;

                        for (Player notify : main.getNotifyPlayers()) {
                            notify.sendMessage(ChatColor.GRAY + "[/shopspy] " + ChatColor.WHITE + username + ChatColor.GRAY + " bought " + ChatColor.WHITE + amount + "x " + name);
                        }

                        final String uuid = player.getUniqueId().toString();

                        // log purchase in purchases.yml
                        ConfigurationSection playerSection;
                        if (!purchaseSection.isConfigurationSection(uuid)) {
                            playerSection = purchaseSection.createSection(uuid);
                            playerSection.set("username", username);
                        } else playerSection = purchaseSection.getConfigurationSection(uuid);

                        String oldUsername = playerSection.getString("username");
                        if (!oldUsername.equals(username) || !playerSection.isConfigurationSection(oldUsername.toLowerCase())) {

                            // remove old user section and create a new one
                            userSection.set(oldUsername, null);

                            userSection.createSection(username.toLowerCase());
                            userSection.set(username.toLowerCase() + ".username", username);
                            userSection.set(username.toLowerCase() + ".uuid", uuid);
                        }

                        long time = System.currentTimeMillis();
                        if (!playerSection.isConfigurationSection("purchases"))
                            playerSection.createSection("purchases");
                        ConfigurationSection purchaseSection = playerSection.getConfigurationSection("purchases").createSection(time + "");
                        purchaseSection.set("itemId", item.getName());
                        purchaseSection.set("itemName", name);
                        purchaseSection.set("amount", amount);

                        // save config
                        try {
                            main.getPurchasesConfig().save(main.getPurchasesFile());
                        } catch (IOException e2) {
                            e2.printStackTrace();
                        }

                        // remove money from player and make sure they can't access the store while it's being removed
                        final long pr = price;

                        if (item.isPremium()) {
                            //final String uuid = player.getUniqueId().toString();
                            player.setMetadata("store", new FixedMetadataValue(main, 0));
                            main.getServer().getScheduler().runTaskAsynchronously(main, new Runnable() {
                                public void run() {

                                    main.getCrystals().removeCrystals(username, pr);

                                    main.getServer().getScheduler().scheduleSyncDelayedTask(main, new Runnable() {
                                        public void run() {

                                            String name;
                                            if (item.getIcon().getItemMeta().hasDisplayName())
                                                name = item.getIcon().getItemMeta().getDisplayName();
                                            else name = main.convertItemMaterial(item.getIcon().getType());

                                            player.sendMessage(ChatColor.YELLOW + "● " + ChatColor.GOLD + "Bought " + ChatColor.GRAY + amount + "x " + name + ChatColor.GOLD + "!");
                                            player.removeMetadata("store", main);
                                        }
                                    });

                                }
                            });
                        } else if (item.isCosmetic()) {
                            player.setMetadata("store", new FixedMetadataValue(main, 0));
                            main.getServer().getScheduler().runTaskAsynchronously(main, new Runnable() {
                                public void run() {

                                    main.getArcadeCoins().removeCoins(username, pr);

                                    main.getServer().getScheduler().scheduleSyncDelayedTask(main, new Runnable() {
                                        public void run() {

                                            String name;
                                            if (item.getIcon().getItemMeta().hasDisplayName())
                                                name = item.getIcon().getItemMeta().getDisplayName();
                                            else name = main.convertItemMaterial(item.getIcon().getType());

                                            player.sendMessage(ChatColor.YELLOW + "● " + ChatColor.GOLD + "Bought " + ChatColor.GRAY + amount + "x " + name + ChatColor.GOLD + "!");
                                            player.removeMetadata("store", main);
                                        }
                                    });

                                }
                            });
                        } else {
                            player.playSound(player.getLocation(), Sound.UI_BUTTON_CLICK, 1, 1);
                            player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_BELL, 1, 1);
                            if (EssentialsHook.getEss() != null) {
                                EssentialsHook.takeMoney(username, new BigDecimal(pr));
                            }

                            if (item.getIcon().getItemMeta().hasDisplayName()) {
                                name = item.getIcon().getItemMeta().getDisplayName();
                            } else {
                                name = main.convertItemMaterial(item.getIcon().getType());
                            }

                            player.sendMessage(ChatColor.YELLOW + "● " + ChatColor.GOLD + "Bought " + ChatColor.GRAY + amount + "x " + name + ChatColor.GOLD + "!");
                            player.removeMetadata("store", main);

                            if (item.getIcon().getItemMeta().hasDisplayName()) {
                                name = item.getIcon().getItemMeta().getDisplayName();
                            } else {
                                name = main.convertItemMaterial(item.getIcon().getType());
                            }

                            player.sendMessage(ChatColor.YELLOW + "● " + ChatColor.GOLD + "Bought " + ChatColor.GRAY + amount + "x " + name + ChatColor.GOLD + "!");
                            player.removeMetadata("store", main);
                        }

                        player.closeInventory();

                        return;
                    } else if (slot == 15 && inv.getItem(slot) != null) {
                        // Add 1
                        quantity++;
                        player.playSound(player.getLocation(), Sound.UI_BUTTON_CLICK, 1, 2);

                    } else if (slot == 16 && inv.getItem(slot) != null) {

                        // Add 16
                        quantity += 16;
                        player.playSound(player.getLocation(), Sound.UI_BUTTON_CLICK, 1, 2);

                    } else if (slot == 17 && inv.getItem(slot) != null) {

                        // Add 64
                        quantity += 64;
                        player.playSound(player.getLocation(), Sound.UI_BUTTON_CLICK, 1, 2);

                    } else if (slot == 11 && inv.getItem(slot) != null) {

                        // Remove 1
                        player.playSound(player.getLocation(), Sound.UI_BUTTON_CLICK, 1, 2);
                        if (player.getMetadata("buyingQuantity").get(0).asInt() > 1) {
                            quantity--;
                        }

                    } else if (slot == 10 && inv.getItem(slot) != null) {

                        // Remove 16
                        player.playSound(player.getLocation(), Sound.UI_BUTTON_CLICK, 1, 2);
                        if (player.getMetadata("buyingQuantity").get(0).asInt() > 16) {
                            quantity -= 16;
                        } else {
                            quantity = -(player.getMetadata("buyingQuantity").get(0).asInt() - 1);
                        }

                    } else if (slot == 9 && inv.getItem(slot) != null) {

                        // Remove 64
                        player.playSound(player.getLocation(), Sound.UI_BUTTON_CLICK, 1, 2);
                        if (player.getMetadata("buyingQuantity").get(0).asInt() > 64) {
                            quantity -= 64;
                        } else {
                            quantity = -(player.getMetadata("buyingQuantity").get(0).asInt() - 1);
                        }

                    } else {
                        return;
                    }

                    // update item in slot 13
                    StoreCategory category = main.getStoreCategory(player.getMetadata("storecategory").get(0).asInt());
                    StoreItem item = category.getStoreItem(player.getMetadata("storeitem").get(0).asInt());

                    main.updateBuyItem(player, item, quantity, money, premium, cosmetic);
                }
            }
        }

    }
}
