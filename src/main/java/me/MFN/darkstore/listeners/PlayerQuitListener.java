package me.MFN.darkstore.listeners;

import me.MFN.darkstore.Main;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.Plugin;

public class PlayerQuitListener implements Listener {

	private Main main;
	
	public PlayerQuitListener(Main main) {
		this.main = main;
	}
	
	@EventHandler
	public void quit(PlayerQuitEvent e) {

		e.getPlayer().removeMetadata("store", main);
		e.getPlayer().removeMetadata("storemenu", main);
		e.getPlayer().removeMetadata("storecategory", main);
		e.getPlayer().removeMetadata("storeitem", main);
		e.getPlayer().removeMetadata("money", main);
		e.getPlayer().removeMetadata("premium", main);
		e.getPlayer().removeMetadata("buyingQuantity", main);
		
	}
	
}
